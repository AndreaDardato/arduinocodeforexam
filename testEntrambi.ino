#include <Adafruit_BMP085.h>
#include <LiquidCrystal.h>
#include <DHT.h>

LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

DHT dht(7, DHT11);

#define PIN_LED 9

//creo il nome al sensore barometrico
Adafruit_BMP085 pressureSensor;


void setup() {
  // put your setup code here, to run once:
 
  //inizializzo la connessione via monitor seriale
  Serial.begin(9600);

  dht.begin();

  lcd.begin(16, 2);

  //starto il pin come dispositivo di output
  pinMode(PIN_LED, OUTPUT);
  digitalWrite(PIN_LED, HIGH);




  //usiamo il pin AREF per la vonversione da anaogico a digitale
  analogReference(EXTERNAL);  //indico che devo usare la tensione presente sul pin AREF (da usare per conversione)
  //AREF è collegata lla tensione 3.3V

  lcd.setCursor(0, 0);
  lcd.print("By CondorTech");


  delay(4000);
  lcd.clear();

 
}

void loop() {
  // put your main code here, to run repeatedly:

  delay(1000);
  float temp = dht.readTemperature();
  int humid = dht.readHumidity();
  //int atmoPressure = pressureSensor.readPressure();
  //long currentPressure = pressureSensor.GetPressure();
  if(!pressureSensor.begin()) {
    Serial.println("Could not find a valid sensor");
    while(1) {}
  }

  long pressure = pressureSensor.readPressure() * 0.01;

  Serial.println(pressure);

  
  //Serial.println("Valori letti dal sensore B180");
  //Serial.println("temperatura sensore B180: " + String(pressureSensTemp));
  //Serial.println("altitudine sensore B180: " + String(altitude));
  //Serial.println("pressione sensore B180: " + String(atmoPressure));
  //Serial.println("pressione a livello del mare sensore B180: " + String(seaLevelPressure));

  Serial.println(";" + String(temp) + ";" + String(humid) + ";" + String(pressure) + ";");

  //stampo i dati sul display lcd
  lcd.setCursor(0, 0);
  lcd.print("Temp:    " + String(temp) + char(0xDF) + "C");
  lcd.setCursor(0, 1);
  lcd.print("Pressione:" + String(pressure) + "mb");

  delay(3000);

  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Pressione:" + String(pressure) + "mb");
  lcd.setCursor(0, 1);
  lcd.print("Umidita:     " + String(humid) + "%");
  delay(3000);

  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Umidita:     " + String(humid) + "%");
  lcd.setCursor(0, 1);
  lcd.print("Temp:    " + String(temp) + char(0xDF) + "C");
 
 

  delay(3000);



}
